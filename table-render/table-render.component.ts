import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Column} from '../../../helpers/json-parser';
import {OdkCentralService} from '../../../services/core/odk-central.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import {Paginator} from 'primeng/paginator';
import {filter} from "rxjs/operators";
import {HttpParams} from '@angular/common/http';
import {ConfirmationService, MessageService} from 'primeng/api';
import {UtilitiesService} from "../../../services/custom/utilities.service";
import {AuthService} from "../../../services/core/auth.service";

@Component({
  selector: 'app-table-render',
  templateUrl: './table-render.component.html',
  styleUrls: ['./table-render.component.scss']
})
export class TableRenderComponent implements OnInit {
  @Input() rows: object[];
  @Input() columns: Column[];
  @Input() columnsToDisplayInFilter: any[];
  @Input() selectedColumnsToDisplayInFilter: any[];
  @Input() loading: boolean;
  @Input() isShowToolBar: boolean = false;
  @Input() isExportingExcel: boolean = false;
  @Input() isExportingSPSS: boolean = false;
  @Input() isExportingZip: boolean = false;
  @Input() isDeletingMultiple: boolean = false;
  @Input() showColumnsFilter: boolean = false;
  @Input() showAddButton: boolean = false;
  @Input() showCheckboxColumn: boolean = false;
  @Input() showActionColumn: boolean = false;
  @Input() showImportExcelButton: boolean = false;
  @Input() showExportZipped: boolean = false;
  @Input() showExportSPSS: boolean = false;
  @Input() exportStatus: string;
  @Input() globalFilterFields: string[];
  @Input() options: any[];
  @Input() images: any[];
  @Input() multiSelectOptions: any[];
  @Input() showDeleteMultipleButton: boolean;
  @Input() checkedRows: any[];
  @Output() rowChecked: EventEmitter<any> = new EventEmitter();
  @Input() tableName: string;
  @Input() totalNumberOfRecords: number;
  @Input() collectionName: string;
  @Input() addButtonText: string;
  @Input() saveButton: boolean;
  @Input() stopJobButton: boolean;
  @Input() startJobButton: boolean;
  @Input() backendPagination: boolean = false;
  @Input() showDefaultDialogForDetails = true;
  @Input() showDetailsDialogType = "data";
  @Input() isCollectionTaggable = false;
  @Output() onEdit: EventEmitter<any> = new EventEmitter();
  @Output() onSave: EventEmitter<any> = new EventEmitter();
  @Output() onShow: EventEmitter<any> = new EventEmitter();
  @Output() onDelete: EventEmitter<any> = new EventEmitter();
  @Output() onDeleteSingleRecord: EventEmitter<any> = new EventEmitter();
  @Output() onDeleteMultiple: EventEmitter<any> = new EventEmitter();
  @Output() onAddNew: EventEmitter<any> = new EventEmitter();
  @Output() onStopJob: EventEmitter<any> = new EventEmitter();
  @Output() onStartJob: EventEmitter<any> = new EventEmitter();
  @Output() onExportExcel: EventEmitter<any> = new EventEmitter();
  @Output() onExportZippedFile: EventEmitter<any> = new EventEmitter();
  @Output() onExportFilteredDataExcel: EventEmitter<any> = new EventEmitter();
  @Output() onExportSPSSFile: EventEmitter<any> = new EventEmitter();
  @Output() onImportExcel: EventEmitter<any> = new EventEmitter();
  @Output() onPageChange: EventEmitter<any> = new EventEmitter();
  @Output() onShowTagDialog: EventEmitter<any> = new EventEmitter();

  selectedImage: SafeUrl = "";
  isLoadingImage = false;
  pageSize: number = 25;
  max: number = 25;
  offset: number = 0;
  first: number = 0;
  currentPage: number = 0;
  selected: any[] = [];
  showDialog: boolean = false;
  doubleClickedRow = {}
  disableEmptyColumnsStatus = false;
  dialogDisplayPosition: boolean;
  dialogPosition: string;

  constructor(private router : Router, private odkCentralService: OdkCentralService, private sanitizer: DomSanitizer,
              private confirmationService: ConfirmationService, private utilitiesService: UtilitiesService, private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  edit(row: object[]) {
    this.onEdit.emit(row);
  }

  show(row: object[]) {
    this.doubleClickedRow = row;
    this.showDialog = true;
    this.onShow.emit(row);
  }

  save(row: object[]){
    this.onSave.emit(row);
  }
  delete(row: object[]) {
    this.onDelete.emit(row);
  }

  deleteMultipleButton(event: any) {
    this.confirm(event);
  }
  create() {
    this.onAddNew.emit();
  }
  exportExcel() {
    this.onExportExcel.emit();
  }

  exportZippedFile(){
    this.onExportZippedFile.emit();
  }

  exportSPSSFile(){
    this.onExportSPSSFile.emit();
  }

  importExcel() {
    this.onImportExcel.emit();
  }
  onCheckboxChangeFn(row) {
    this.rowChecked.emit(row);
    // console.log(this.rowChecked);
    // $event.stopPropagation();
  }

  unSelect(row: any) {
    this.rowChecked.emit(row);
  }

  onSelect(selected) {
    this.selected = [];
    this.selected.push(selected);
  }

  onTableFilter(e: any) {
    this.onExportFilteredDataExcel.emit(e)
  }

  // isDate(col: Column) {
  //   return col.type == 'data' || col.name.toLowerCase().includes('date') || col.name.toLowerCase().includes('lastupdated');
  // }

  isDate(value) {
    // return value instanceof Date;
    return this.utilitiesService.isDate(value)
  }

  isObject(value) {
    return value instanceof Object && !Array.isArray(value);
  }

  isArray(value) {
    return Array.isArray(value);
  }

  isString(value) {
    return typeof value === 'string';
  }

  isUniqueId(value){
    if(typeof value === 'string') {
      return value.startsWith("uuid:")
    }else{
      return;
    }
  }

  isId(value){
    const idRegex = /^[0-9a-f]{24}$/i;
    return idRegex.test(value);
  }

  isImage(value) {
    if (value == null || value == undefined || typeof value !== "string") return;
    let rowData = value.toString().toLowerCase();
    return rowData.includes('.jpg') || rowData.includes('.jpeg') || rowData.includes('.png') || rowData.includes('.gif') || rowData.includes('.bmp') || rowData.includes('.svg');
  }

  getId() {
    if(this.columns == null) return null;
    let columns = this.columns.filter((column: Column) => column.label.includes('Id'));
    if (columns.length > 0) {
      return columns[0].id;
    }
  }

  getImage(collectionName, row: any, colName) {
    this.selectedImage = "";
    if (!row.meta) return;

    let instanceID = row.meta.instanceID;
    let imageName = row[colName];

    this.isLoadingImage = true;
    const params = new HttpParams()
    params.set("width", 640);
    params.set("height", 480);
    this.odkCentralService.getCentralImage(imageName, params).subscribe((response) => {
      this.isLoadingImage = false
      let fileExtension = imageName.toString().split(".")[1] ?? "jpeg";
      const blobUrl = new Blob([response], { type: `image/${fileExtension}` });
      const imageUrl = URL.createObjectURL(blobUrl);
      this.selectedImage = this.sanitizer.bypassSecurityTrustUrl(imageUrl);
    }, error => {
      this.isLoadingImage = false
      console.log(error);
    });
  }

  frontEndPaginator($event: any) {
    if(!this.backendPagination) {
      this.first = $event.first;
      this.currentPage = $event.page ?? this.first / this.pageSize;
      this.pageSize = $event.rows;
      this.offset = this.currentPage * this.pageSize;
      this.max = $event.rows;

      let dataShown = this.rows.length / this.pageSize;
      if (this.currentPage >= dataShown) {
        this.backendPagination = true;
        this.backEndPaginator({page: this.currentPage, rows: this.pageSize});
      } else {
        this.backendPagination = false;
      }
    }
  }

  backEndPaginator($event: any) {
    console.log($event)
    this.currentPage = $event.page
    this.pageSize = $event.rows
    if($event.first!=undefined) this.first = $event.first
    this.offset = this.currentPage * this.pageSize
    this.max = $event.rows

    this.onPageChange.emit({page: this.currentPage + 1, limit: this.pageSize});
  }

  stopJob(rowData: any) {
    this.onStopJob.emit(rowData)
  }

  startJob(rowData: any) {
    this.onStartJob.emit(rowData)
  }

  permittedColumns(column) {
    const columns = ['meta', 'unique_id', 'image_attendance', 'image_participants', 'adolescents']
    return !columns.includes(column)
  }

  permittedRows(value) {
    if(!this.isImage(value)) return true;
  }

  deleteSingleRecord(selectedRow){
    this.onDeleteSingleRecord.emit(selectedRow);
  }


  confirm(event: Event, selectedRow?, deleteType?: string) {
    this.confirmationService.confirm({
      target: event.target,
      message: 'Are you sure that you want to proceed?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        deleteType === 'singleDelete' ? this.deleteSingleRecord(selectedRow) : this.onDeleteMultiple.emit(this.checkedRows);
      },
      reject: () => {
        console.log("Rejected")
      }
    });
  }

  onSelectAllHandler(e){
    if(e.checked){
      this.showDeleteMultipleButton = true;
      this.checkedRows = this.rows
    } else{
      this.showDeleteMultipleButton = false;
      this.checkedRows = []
    }
  }

  getObjectEntries(obj: any): [string, any][] {
    return Object.entries(obj);
  }

  getIndex(index: number) {
    if(this.offset!=0 && this.backendPagination) {
      console.log("offset", this.offset);
      console.log("pageSize", this.pageSize);
      console.log("index", index);
      let off = this.offset;
      let page = this.pageSize;
      return index + off - page + 1;
    }
    else return index + 1;
  }

  getObjectKeys(obj: any): string[] {
    return Object.keys(obj);
  }

  canDelete() {
    return this.authService.canDelete(this.tableName);
  }

  getKeyOrId(col){
    if(!this.rows || this.rows.length == 0) return col.id ?? col.key;
    const rowKeys = Object.keys(this.rows[0]);
    if(rowKeys.includes(col.id)) return col.id;
    if(rowKeys.includes(col.key)) return col.key;

    return  col.id ?? col.key
  }

  hideEmptyCellsOnAllTables(){
    console.log("hi")
    const tables = document.querySelectorAll("[id^='pr_id'][id$='-table']");
    tables.forEach((table) => this.hideEmptyCols(table))
  }

  showEmptyCellsOnAllTables(){
    const tables = document.querySelectorAll("[id^='pr_id'][id$='-table']");
    tables.forEach((table) => this.showEmptyCols(table))
  }

  hideEmptyCols(table) {
    var thList = table.getElementsByTagName("th");
    var numCols = thList.length;

    setTimeout(() => {
      for (var i = 1; i <= numCols; i++) {
        var empty = true;
        var tdList = table.querySelectorAll("td:nth-child(" + i + ")");

        tdList.forEach(function (el) {
          if (el.classList.contains("checkbox")) return;

          const el1 = el.querySelector("div") === null ? el : el.querySelector("div").querySelector("div");

          if (el1 && el1.textContent !== "") {
            empty = false;
            return false;
          }
        });

        if (empty && tdList.length > 0) {
          tdList.forEach(function (el) {
            if (el.classList.contains("checkbox")) return;

            const el1 = el.querySelector("div") === null ? el : el.querySelector("div").querySelector("div");

            if(el1.parentElement.parentElement != null){
               el.classList.add("d-none")
               el.classList.add("empty-cell")
            }
          });

          !thList[i - 1].classList.contains("checkbox") ? this.addEmptyCssClasses(thList[i - 1]) : undefined;

        }
      }
    }, 100)
  }

  addEmptyCssClasses(element){
    element.classList.add("d-none");
    element.classList.add("empty-cell");
  }
  showEmptyCols(table) {
    var thList = table.getElementsByTagName("th");
    var numCols = thList.length;

    for (var i = 1; i <= numCols; i++) {
        var empty = true;
        var tdList = table.querySelectorAll("td:nth-child(" + i + ")");
        var hiddenCells = Array.from(tdList).filter(function(element) {
          return (element as HTMLElement).classList.contains('empty-cell');
        });

        if (hiddenCells.length > 0) {
          hiddenCells.forEach(function (el) {
            (el as HTMLElement).classList.remove('d-none');
            (el as HTMLElement).classList.remove('empty-cell');
          });
        }

        var hiddenCellHeaders = Array.from(thList).filter(function(element) {
          return (element as HTMLElement).classList.contains('empty-cell');
        });
        if (hiddenCellHeaders.length > 0) {
          hiddenCellHeaders.forEach(function (el) {
            (el as HTMLElement).classList.remove('d-none');
            (el as HTMLElement).classList.remove('empty-cell');
          });
        }
      }
  }

  disableEmptyColumnsChange(ev){
    this.disableEmptyColumnsStatus ? this.hideEmptyCellsOnAllTables() : this.showEmptyCellsOnAllTables();
  }

  showPositionDialog(position: string) {
    this.dialogPosition = position;
    this.dialogDisplayPosition = true;
    this.onShowTagDialog.emit();
  }

  protected readonly filter = filter;
  protected readonly indexedDB = indexedDB;
}
